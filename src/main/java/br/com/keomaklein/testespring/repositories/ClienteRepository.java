package br.com.keomaklein.testespring.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.keomaklein.testespring.entities.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{

	List<Cliente> findAll();
	

	@Query(value = "SELECT c FROM Cliente c")
	Page<Cliente> getClientes(Pageable pageable);
	
	
	Optional<Cliente> findByCpf(String cpf);
	
	List<Cliente> findByNome(String nome);
	
	@Query("select c from Cliente c where c.nome like %:nome%")
	List<Cliente> findByParcialNome(@Param("nome") String nome);
	
	Cliente findByCpfAndNome(String cpf, String nome);

}
