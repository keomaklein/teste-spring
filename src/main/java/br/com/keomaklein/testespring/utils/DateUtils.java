package br.com.keomaklein.testespring.utils;

import java.util.Calendar;
import java.util.Date;

import br.com.keomaklein.testespring.entities.Cliente;

public class DateUtils {
	public static int getIdade(Cliente c) {
		Date data = c.getDataNascimento();
		Calendar cData = Calendar.getInstance();
		Calendar cHoje= Calendar.getInstance();
		cData.setTime(data);
		cData.set(Calendar.YEAR, cHoje.get(Calendar.YEAR));
		int idade = cData.after(cHoje) ? -1 : 0;
		cData.setTime(data);
		idade += cHoje.get(Calendar.YEAR) - cData.get(Calendar.YEAR);
		return idade;
	}
}
