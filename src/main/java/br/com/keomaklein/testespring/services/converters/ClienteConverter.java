package br.com.keomaklein.testespring.services.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.keomaklein.testespring.entities.Cliente;
import br.com.keomaklein.testespring.services.vos.ClienteVO;

@Component
public class ClienteConverter {
	
	public static ClienteVO parse(Cliente cliente) {
		if (cliente == null) {
			return null;
		}

		ClienteVO clienteVO = new ClienteVO();
		clienteVO.setId(cliente.getId());
		clienteVO.setCpf(cliente.getCpf());
		clienteVO.setNome(cliente.getNome());
		clienteVO.setDataNascimento(cliente.getDataNascimento());
		
		return clienteVO;
	}

	
	public static Cliente parse(ClienteVO clienteVO) {
		if (clienteVO == null) {
			return null;
		}

		Cliente cliente = new Cliente();
		cliente.setId(clienteVO.getId());
		cliente.setCpf(clienteVO.getCpf());
		cliente.setNome(clienteVO.getNome());
		cliente.setDataNascimento(clienteVO.getDataNascimento());
		
		return cliente;
	}
	
	public static List<ClienteVO> parseList(List<Cliente> clientes) {
		List<ClienteVO> voList = new ArrayList<ClienteVO>();
		for (Cliente cliente : clientes) {
			voList.add(parse(cliente));
		}
		return voList;
	}

}
