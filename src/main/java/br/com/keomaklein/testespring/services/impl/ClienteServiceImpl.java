package br.com.keomaklein.testespring.services.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.keomaklein.testespring.entities.Cliente;
import br.com.keomaklein.testespring.exceptions.ClienteExisteNaBaseException;
import br.com.keomaklein.testespring.exceptions.ClienteNaoEncontradoException;
import br.com.keomaklein.testespring.exceptions.CpfInvalidoException;
import br.com.keomaklein.testespring.repositories.ClienteRepository;
import br.com.keomaklein.testespring.services.ClienteService;
import br.com.keomaklein.testespring.utils.CpfUtils;
import br.com.keomaklein.testespring.utils.DateUtils;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public List<Cliente> findAll() {
		List<Cliente> clientes = clienteRepository.findAll();
		clientes.forEach(cliente -> calcularIdade(cliente));
		return clientes;
	}

	@Override
	public Page<Cliente> findClientes(int page, int pageSize) {
		Pageable paginacao = PageRequest.of(page > 0 ? page - 1 : page, pageSize, Sort.by("id").descending());
		Page<Cliente> clientesPage = clienteRepository.getClientes(paginacao);
		clientesPage.getContent().forEach(cliente -> calcularIdade(cliente));
		return clientesPage;
	}

	@Override
	public Cliente findById(int id) {
		return calcularIdade(clienteRepository.findById(id));
	}

	@Override
	public Cliente findByCpf(String cpf) {
		if (!CpfUtils.isCPFValid(cpf)) {
			throw new CpfInvalidoException();
		}
		return calcularIdade(clienteRepository.findByCpf(cpf));
	}

	@Override
	public List<Cliente> findByNome(String nome) {
		return calcularIdade(clienteRepository.findByNome(nome));
	}

	@Override
	public List<Cliente> findByParcialNome(String nome) {
		return calcularIdade(clienteRepository.findByParcialNome(nome));
	}

	@Override
	public Cliente findByCpfAndNome(String cpf, String nome) {
		if (Objects.nonNull(cpf) && !CpfUtils.isCPFValid(cpf)) {
			throw new CpfInvalidoException();
		}
		return calcularIdade(clienteRepository.findByCpfAndNome(cpf, nome));
	}

	@Override
	public Cliente incluir(Cliente cliente) {
		if (cliente.getId() > 0 && clienteRepository.existsById(cliente.getId())) {
			throw new ClienteExisteNaBaseException();
		}
		return calcularIdade(clienteRepository.save(cliente));
	}
	
	@Override
	public Cliente update(int id, Cliente cliente) {		
		if (!clienteRepository.existsById(id)) throw new ClienteNaoEncontradoException();
		cliente.setId(id);
		return calcularIdade(clienteRepository.save(cliente));
	}
	

	@Override
	public Cliente patch(int id, Cliente clienteNew) {
		Optional<Cliente> clienteOptOld = clienteRepository.findById(id);
		if (clienteOptOld.isEmpty()) throw new ClienteNaoEncontradoException();
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		Cliente cliente = clienteOptOld.get();
		mapper.map(clienteNew, cliente);
		return calcularIdade(clienteRepository.save(cliente));
	}

	@Override
	public boolean delete(int id) {
		Optional<Cliente> oldCliente = clienteRepository.findById(id);
		if (oldCliente.isPresent())
			clienteRepository.delete(oldCliente.get());
		return oldCliente.isPresent();
	}

	private Cliente calcularIdade(Optional<Cliente> clienteOptional) {
		Cliente cliente = null;
		if (clienteOptional.isPresent()) {
			cliente = clienteOptional.get();
			cliente.setIdade(DateUtils.getIdade(cliente));
		}
		return cliente;
	}

	private Cliente calcularIdade(Cliente cliente) {
		if (Objects.nonNull(cliente))
			cliente.setIdade(DateUtils.getIdade(cliente));
		return cliente;
	}

	private List<Cliente> calcularIdade(List<Cliente> clientes) {
		if (!clientes.isEmpty()) {
			clientes.forEach(cliente -> cliente.setIdade(DateUtils.getIdade(cliente)));
		}
		return clientes;
	}
}
