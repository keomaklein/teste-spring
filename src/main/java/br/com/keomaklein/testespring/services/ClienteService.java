package br.com.keomaklein.testespring.services;

import java.util.List;

import org.springframework.data.domain.Page;

import br.com.keomaklein.testespring.entities.Cliente;

public interface ClienteService {	
	
	List<Cliente> findAll();
	
	Page<Cliente> findClientes(int page, int pageSize);

	Cliente findById(int id);
	
	Cliente incluir(Cliente cliente);
	
	Cliente update(int id, Cliente cliente);
	
	Cliente patch(int id, Cliente cliente);
	
	boolean delete(int id);
	
	Cliente findByCpf(String cpf);
	
	List<Cliente> findByNome(String nome);
	
	List<Cliente> findByParcialNome(String nome);
	
	Cliente findByCpfAndNome(String cpf, String nome);
}
