package br.com.keomaklein.testespring.controllers;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.keomaklein.testespring.entities.Cliente;
import br.com.keomaklein.testespring.exceptions.ClienteExisteNaBaseException;
import br.com.keomaklein.testespring.exceptions.ClienteNaoEncontradoException;
import br.com.keomaklein.testespring.exceptions.CpfInvalidoException;
import br.com.keomaklein.testespring.services.ClienteService;
import br.com.keomaklein.testespring.services.converters.ClienteConverter;
import br.com.keomaklein.testespring.services.vos.ClienteVO;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	public ResponseEntity<?> getAllClientes() {
		List<Cliente> clientes = clienteService.findAll();
		return clientes.isEmpty() ?
				ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cliente não encontrado!") :
				ResponseEntity.status(HttpStatus.OK).body(clientes);		
	}
	
	@GetMapping("/paginado")
	public ResponseEntity<?> getPageClientes(@RequestParam(name = "page", required = true) int page, @RequestParam(name = "pageSize", required = true) int pageSize) {
		try {			
			Page<Cliente> clientes = clienteService.findClientes(page, pageSize);
			return clientes.isEmpty() ?
					ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não retornou clientes!") :
					ResponseEntity.status(HttpStatus.OK).body(clientes);		
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Houve um erro, tente mais tarde novamente!");
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getCliente(@PathVariable("id") int id) {
		Cliente cliente = clienteService.findById(id);
		return Objects.nonNull(cliente) ? 
				ResponseEntity.status(HttpStatus.OK).body(cliente) : 
				ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cliente não encontrado!");		
	}

	
	@GetMapping("/consulta")
	public ResponseEntity<?> getClienteByCpfAndName(@RequestParam(name = "cpf", required = false) String cpf, @RequestParam(name="nome", required = false) String nome) {
		try {			
			Cliente cliente = null;
			if(Objects.nonNull(cpf) && Objects.nonNull(nome)) {
				cliente = clienteService.findByCpfAndNome(cpf, nome);
				if(Objects.nonNull(cliente)) return ResponseEntity.status(HttpStatus.OK).body(cliente);				
			}else if(Objects.nonNull(cpf)) {
				cliente = clienteService.findByCpf(cpf);
				if(Objects.nonNull(cliente)) return ResponseEntity.status(HttpStatus.OK).body(cliente);
			}else if(Objects.nonNull(nome)) {
				List<Cliente> clientes = clienteService.findByParcialNome(nome);
				if(!clientes.isEmpty()) return ResponseEntity.status(HttpStatus.OK).body(clientes);
			}
			return Objects.nonNull(cliente) ? 
					ResponseEntity.status(HttpStatus.OK).body(cliente) : 
					ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cliente não encontrado!");
		} catch (CpfInvalidoException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMensagem());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Houve um erro, tente mais tarde novamente!");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> adicionarCliente(@Valid @RequestBody ClienteVO clienteVO) {
		try {
			Cliente newCliente = clienteService.incluir(ClienteConverter.parse(clienteVO));
			return Objects.nonNull(newCliente) ? 
					ResponseEntity.status(HttpStatus.CREATED).body(newCliente) : 
					ResponseEntity.status(HttpStatus.NOT_FOUND).body("Erro ao incluir cliente!");
		} catch (ClienteExisteNaBaseException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMensagem());
		} catch (DataIntegrityViolationException e) { 
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Houve um erro, verifique se o cpf já esta cadastrado na base!");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Houve um erro, tente mais tarde novamente!");
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> alterarCliente(@PathVariable("id") Integer id, @Valid @RequestBody ClienteVO clienteVO) {																	  
		try {
			Cliente newCliente = clienteService.update(id, ClienteConverter.parse(clienteVO));
			return Objects.nonNull(newCliente) ? 
					ResponseEntity.status(HttpStatus.OK).body(newCliente) : 
					ResponseEntity.status(HttpStatus.NOT_FOUND).body("Erro ao incluir cliente!");
		} catch (ClienteNaoEncontradoException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMensagem());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Houve um erro, tente mais tarde novamente!");
		}
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<?> alterarParcialmenteCliente(@PathVariable("id") Integer id, @RequestBody ClienteVO clienteVO) {
		try {
			Cliente newCliente = clienteService.patch(id, ClienteConverter.parse(clienteVO));
			return Objects.nonNull(newCliente) ? 
					ResponseEntity.status(HttpStatus.OK).body(newCliente) : 
					ResponseEntity.status(HttpStatus.NOT_FOUND).body("Erro ao incluir cliente!");
		} catch (ClienteNaoEncontradoException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMensagem());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Houve um erro, tente mais tarde novamente!");
		}	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> excluirCliente(@PathVariable("id") Integer id) {
		try {
			boolean retornoDelete = clienteService.delete(id);
			return retornoDelete ? 
					ResponseEntity.status(HttpStatus.OK).body("Cliente excluido com sucesso!") : 
					ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cliente não encontrado na base para exclusão!");
		} catch (ClienteNaoEncontradoException e) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMensagem());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Houve um erro, tente mais tarde novamente!");
		}
	}
}
