package br.com.keomaklein.testespring.exceptions;

@SuppressWarnings("serial")
public class CpfInvalidoException extends RuntimeException {

	private String mensagem = "Cpf inválido!";

	public CpfInvalidoException() {		
	}
	
	public CpfInvalidoException(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}
	
}
