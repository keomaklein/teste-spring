package br.com.keomaklein.testespring.exceptions;

@SuppressWarnings("serial")
public class ClienteNaoEncontradoException extends RuntimeException {

	private String mensagem = "Cliente não encontrado!";

	public ClienteNaoEncontradoException() {		
	}
	
	public ClienteNaoEncontradoException(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}
	
}
