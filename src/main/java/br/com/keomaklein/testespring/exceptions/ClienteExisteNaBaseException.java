package br.com.keomaklein.testespring.exceptions;

@SuppressWarnings("serial")
public class ClienteExisteNaBaseException extends RuntimeException {

	private String mensagem = "Cliente existe na base!";

	public ClienteExisteNaBaseException() {		
	}
	
	public ClienteExisteNaBaseException(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}
	
}
